# Zuse OS

An OS for the RISC-V architecture written from scratch

## Why RISC-V?

Why not? There's a lot of OSes written from scratch for the x86 architecture, but not as many for RISC-V.

## What's the plan for now?

Just setup a simple IO-system, processes, web connectivity and memory management.