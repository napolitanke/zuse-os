CC = riscv64-unknown-elf-gcc
CFLAGS = -nostdlib -nostdinc -fno-builtin -fno-stack-protector -nostartfiles -nodefaultlibs

AS = riscv64-unknown-elf-as
ASFLAGS = -g

LDFLAGS = -T -Ttext=0x80000000


build:
	sudo riscv64-unknown-elf-gcc kernel.c -o main.o -r -nostdlib -nostdinc -fno-builtin -fno-stack-protector -nostartfiles
	sudo riscv64-unknown-elf-as -g -o loader.o loader.S
	sudo riscv64-unknown-elf-ld -T link.ld loader.o main.o -o kernel.elf -Ttext=0x80000000
	echo "Build successful!"

qemu: build
	sudo qemu-system-riscv64 -kernel kernel.elf -S -gdb tcp::1234 -d in_asm -D log.txt

obH:
	sudo riscv64-unknown-elf-objdump -h kernel.elf

obT:
	sudo riscv64-unknown-elf-objdump -D kernel.elf | tail -1000

gdb: 
	sudo riscv64-unknown-elf-gdb
