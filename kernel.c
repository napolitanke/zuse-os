// test kernel

int testFunction(){
    int x = 1000;
    return x;
}

int main(){
    int h = testFunction();
    __asm__("li t3, 0x2000");
    __asm__("sd t3, 0(sp)");
    return 0;
}